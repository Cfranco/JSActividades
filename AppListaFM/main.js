(function(){
    //Variables
    var lista = document.getElementById("lista"),
        tareaInput = document.getElementById("tareaInput"),
        btnNuevaTarea = document.getElementById("btn-agregar");

    //Funciones
    var agregarTarea = function(){
        var tarea = tareaInput.value;
            nuevatarea = document.createElement("li"),
            enlace = document.createElement("a"),
            contenido = document.createTextNode(tarea);

            if(tarea ===""){
                tareaInput.setAttribute("placeholder","Agrege una tarea valida");
                tareaInput.className = "error";
                return false;
            }

            //agreramos el contenido al enlace
            enlace.appendChild(contenido);
            //le establecemos un atributo href
            enlace.setAttribute("href", "#");
            //Agregamos el enlace (a) a la nueva tarea (li)
            nuevatarea.appendChild(enlace);
            //Agregamos la nueva tarea a la lista
            lista.appendChild(nuevatarea);

            tareaInput.value = "";

                // Borrando elementos de la lista duda :(
    for (var i = 0; i <= lista.children.length -1; i++) {
        lista.children[i].addEventListener("click", function(){
            this.parentNode.removeChild(this);
        });
    }

    };
    var comprobarInput = function(){
        tareaInput.className="";
        tareaInput.setAttribute("placeholder","Agrega tu tarea");
    };
    var eliminarTarea = function(){
        this.parentNode.removeChild(this);
    };

    //Eventos

    //Agregar Tarea
    btnNuevaTarea.addEventListener("click",agregarTarea);

    //Comprobar input
    tareaInput.addEventListener("click", comprobarInput);

    // Borrando elementos de la lista duda :(
    for (var i = 0; i <= lista.children.length -1; i++) {
        lista.children[i].addEventListener("click", eliminarTarea);
    }
} ());